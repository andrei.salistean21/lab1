public class ford extends car {
    private int year;
    private int manufacturerDiscount;

    public ford(int year, int speed, double regularPrice, String color, int discount) {
        super(speed, regularPrice, color);
        this.year = year;
        this.manufacturerDiscount = discount;
    }

    @Override
    public double getSalesPrice() {
        return (this.regularPrice - this.regularPrice / this.manufacturerDiscount);
    }
}
