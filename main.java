
public class main {
    public static void main(String[] args) {

        // ex1.Ex1();

        // System.out.println("\n" + ex2.Ex2());

        // ex3.Ex3();

        // ex4 pers = new ex4("Mihai", 77);
        // System.out.println(pers.getName() + " " + pers.getAge());
        truck t = new truck(1000, 100, 20000, "red");
        sedan s = new sedan(25, 100, 20000, "blue");
        ford f1 = new ford(2015, 100, 20000, "red", 20);
        ford f2 = new ford(2017, 100, 20000, "red", 9);
        car c = new car(100, 20000, "red");

        System.out.println(t.getSalesPrice());
        System.out.println(s.getSalesPrice());
        System.out.println(f1.getSalesPrice());
        System.out.println(f2.getSalesPrice());
        System.out.println(c.getSalesPrice());
    }
}
