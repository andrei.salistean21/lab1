public class truck extends car {
    private int weight;

    public truck(int weight, int speed, double regularPrice, String color) {
        super(speed, regularPrice, color);
        this.weight = weight;

    }

    @Override
    public double getSalesPrice() {
        if (this.weight > 2000)
            return (this.regularPrice - this.regularPrice / 10);
        else
            return (this.regularPrice - this.regularPrice / 10 * 2);
    }
}
