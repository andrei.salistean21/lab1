public class sedan extends car {
    private int lenght;

    public sedan(int lenght, int speed, double regularPrice, String color) {
        super(speed, regularPrice, color);
        this.lenght = lenght;
    }

    @Override
    public double getSalesPrice() {
        if (this.lenght > 20)
            return (this.regularPrice - this.regularPrice / 20);
        else
            return (this.regularPrice - this.regularPrice / 10);
    }
}
