public class ex3 {
    public static void Ex3() {
        int prim1 = 3;

        for (char i = 4; i < 100; ++i) {
            if (isPrim(i)) {
                System.out.println("\n(" + prim1 + " , " + (short) i + ")");
                prim1 = i;
            }
        }
    }

    private static boolean isPrim(int pr) {
        int nrMult = 0;
        for (char j = 1; j <= pr; ++j) {
            if (pr % j == 0)
                ++nrMult;
        }
        if (nrMult == 2)
            return true;
        else
            return false;
    }
}
